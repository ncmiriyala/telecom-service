# README #
#TELECOM SERVICE APP
This application is used to store phone numbers associated to customers and operations to manage them.  

Capabilities provided
- Get all phone numbers
- Get all phone numbers of a single customer
- Activate a phone number

### Requirements
Have below tools installed on your machine
 - Java 17
 - gradle
 - Junit


### How do I get set up? ###

~~~ bash
$ ./gradlew clean build
~~~

Running Tests

~~~ bash
$ ./gradlew test
~~~

