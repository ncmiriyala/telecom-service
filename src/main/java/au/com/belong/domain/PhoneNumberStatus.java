package au.com.belong.domain;

public enum PhoneNumberStatus {
    ACTIVE,
    INACTIVE
}
