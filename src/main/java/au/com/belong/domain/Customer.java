package au.com.belong.domain;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Customer {
    private final long customerId;
    private final Set<PhoneNumber> phoneNumbers;

    public Customer(long customerId) {
        assert customerId > 0 : "CustomerId is invalid";
        this.customerId = customerId;
        this.phoneNumbers = new HashSet<>();
    }

    public void addPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumbers.add(phoneNumber);
        if (phoneNumber.getCustomer() != this) {
            phoneNumber.setCustomer(this);
        }
    }

    public long getCustomerId() {
        return customerId;
    }

    public Set<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return customerId == customer.customerId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerId);
    }
}
