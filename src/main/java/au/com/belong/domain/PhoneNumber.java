package au.com.belong.domain;

import au.com.belong.common.PhoneNumberUtil;

import java.io.Serializable;
import java.util.Objects;

public class PhoneNumber implements Serializable {
    private final String number;
    private PhoneNumberStatus phoneNumberStatus;
    private Customer customer;

    /**
     * Initial status of phone number is Inactive.
     * @param number
     * @param customerId
     */
    public PhoneNumber(String number, long customerId) {
        assert number != null && !number.isEmpty() && PhoneNumberUtil.isValidAustralianPhoneNumber(number) : "Phone Number is invalid";
        this.number = number;
        this.customer = new Customer(customerId);
        this.phoneNumberStatus = PhoneNumberStatus.INACTIVE;
    }

    public String getNumber() {
        return number;
    }

    public Customer getCustomer() {
        return customer;
    }

    public PhoneNumberStatus getPhoneNumberStatus() {
        return phoneNumberStatus;
    }

    public void activate() {
        this.phoneNumberStatus = PhoneNumberStatus.ACTIVE;
    }

    public void deactivate() {
        this.phoneNumberStatus = PhoneNumberStatus.INACTIVE;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        customer.getPhoneNumbers().add(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneNumber that = (PhoneNumber) o;
        return Objects.equals(number, that.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    @Override
    public String toString() {
        return "PhoneNumber{" +
                "number='" + number + '\'' +
                ", phoneNumberStatus=" + phoneNumberStatus +
                ", customer=" + customer +
                '}';
    }
}
