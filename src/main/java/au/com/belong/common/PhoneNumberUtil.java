package au.com.belong.common;

import java.util.regex.Pattern;

public class PhoneNumberUtil {
    public static final String australiaPhoneNumberRegex = "^(?:\\+?(61))? ?(?:\\((?=.*\\)))?(0?[2-57-8])\\)? ?(\\d\\d(?:[- ](?=\\d{3})|(?!\\d\\d[- ]?\\d[- ]))\\d\\d[- ]?\\d[- ]?\\d{3})$";

    public static boolean isValidAustralianPhoneNumber(String phoneNumber) {
        return Pattern.compile(australiaPhoneNumberRegex).matcher(phoneNumber).matches();
    }
}
