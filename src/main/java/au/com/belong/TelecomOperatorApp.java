package au.com.belong;

import au.com.belong.domain.PhoneNumber;
import au.com.belong.service.PhoneNumberService;
import au.com.belong.service.PhoneNumberServiceImpl;

public class TelecomOperatorApp {

    public static final String ONBOARD_PHONE_NUMBER = "0466089798";
    public static final int CUSTOMER_ID = 999898;

    public static void main(String[] args) {
        PhoneNumber phoneNumber=new PhoneNumber(ONBOARD_PHONE_NUMBER, CUSTOMER_ID);
        PhoneNumberService phoneNumberService=new PhoneNumberServiceImpl();
        phoneNumberService.addPhoneNumber(phoneNumber);
        phoneNumberService.activatePhoneNumber(ONBOARD_PHONE_NUMBER);

    }
}
