package au.com.belong.service;

import au.com.belong.domain.PhoneNumber;
import au.com.belong.domain.PhoneNumberStatus;
import au.com.belong.exception.ActivationException;
import au.com.belong.exception.PhoneNumberExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class PhoneNumberServiceImpl implements PhoneNumberService {
    Set<PhoneNumber> allPhoneNumbers = new HashSet<>();

    public static final Logger logger= LoggerFactory.getLogger(PhoneNumberServiceImpl.class);

    @Override
    public Set<PhoneNumber> getAllPhoneNumbers() {
        return allPhoneNumbers;
    }

    @Override
    public void addPhoneNumber(PhoneNumber phoneNumber) {
        if (allPhoneNumbers.contains(phoneNumber)) {
            throw new PhoneNumberExistsException(phoneNumber + " already exists");
        }
        allPhoneNumbers.add(phoneNumber);
        logger.info("Successfully added phone number {}",phoneNumber);
    }

    @Override
    public Set<PhoneNumber> getAllPhoneNumbersByCustomerId(long customerId) {
        return allPhoneNumbers.stream().filter(phoneNumber -> customerId == phoneNumber.getCustomer().getCustomerId())
                .collect(Collectors.toSet());
    }

    @Override
    public void activatePhoneNumber(String number) {
        allPhoneNumbers.stream().filter(phoneNumber -> Objects.equals(phoneNumber.getNumber(), number) && isInActivePhoneNumber(phoneNumber))
                .findAny().ifPresentOrElse(PhoneNumber::activate, () -> {
                    throw new ActivationException(number + " not activated");
                });
        logger.info("Successfully activated phone number {}",number);
    }

    private boolean isInActivePhoneNumber(PhoneNumber phoneNumber) {
        return phoneNumber.getPhoneNumberStatus() == PhoneNumberStatus.INACTIVE;
    }
}
