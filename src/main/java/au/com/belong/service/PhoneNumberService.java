package au.com.belong.service;

import au.com.belong.domain.PhoneNumber;

import java.util.Set;

public interface PhoneNumberService {
    Set<PhoneNumber> getAllPhoneNumbers();

    void addPhoneNumber(PhoneNumber phoneNumber);

    Set<PhoneNumber> getAllPhoneNumbersByCustomerId(long customerId);

    void activatePhoneNumber(String number);
}
