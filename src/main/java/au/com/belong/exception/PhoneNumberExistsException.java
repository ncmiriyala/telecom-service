package au.com.belong.exception;
/**
 * Exception thrown when there is already an existing phone number in the system
 */
public class PhoneNumberExistsException extends RuntimeException {
    public PhoneNumberExistsException(String message) {
        super(message);
    }
}
