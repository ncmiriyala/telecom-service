package au.com.belong.exception;

/**
 * Exception thrown during activation of phone number
 */
public class ActivationException extends RuntimeException {
    public ActivationException(String message) {
        super(message);
    }
}
