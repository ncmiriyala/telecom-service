package au.com.belong.service;

import au.com.belong.domain.PhoneNumber;
import au.com.belong.domain.PhoneNumberStatus;
import au.com.belong.exception.ActivationException;
import au.com.belong.exception.PhoneNumberExistsException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static au.com.belong.domain.TestDataFixture.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PhoneNumberServiceImplTest {
    private PhoneNumberService phoneNumberService;
    private PhoneNumber phoneNumberOne;
    private PhoneNumber phoneNumberTwo;
    private PhoneNumber phoneNumberThree;
    private PhoneNumber phoneNumberFour;

    @BeforeEach
    void setUp() {
        phoneNumberService = new PhoneNumberServiceImpl();
        phoneNumberOne = new PhoneNumber(MOCK_PHONE_NUMBER_1, MOCK_CUSTOMER_ID_1);
        phoneNumberTwo = new PhoneNumber(MOCK_PHONE_NUMBER_2, MOCK_CUSTOMER_ID_2);
        phoneNumberThree = new PhoneNumber(MOCK_PHONE_NUMBER_3, MOCK_CUSTOMER_ID_3);
        phoneNumberFour = new PhoneNumber(MOCK_PHONE_NUMBER_4, MOCK_CUSTOMER_ID_1);
        phoneNumberService.addPhoneNumber(phoneNumberOne);
        phoneNumberService.addPhoneNumber(phoneNumberTwo);
        phoneNumberService.addPhoneNumber(phoneNumberThree);
        phoneNumberService.addPhoneNumber(phoneNumberFour);
    }

    @Test
    void verifySamePhoneNumberIsNotAddedMultipleTimes() {
        PhoneNumber duplicatePhoneNumber = new PhoneNumber(MOCK_PHONE_NUMBER_1, MOCK_CUSTOMER_ID_1);
        PhoneNumberExistsException thrown = Assertions
                .assertThrows(PhoneNumberExistsException.class, () -> phoneNumberService.addPhoneNumber(duplicatePhoneNumber), "PhoneNumberExistsException error was expected");

        assertEquals("PhoneNumber{number='" + MOCK_PHONE_NUMBER_1 + "', phoneNumberStatus=INACTIVE, customer=Customer{customerId=" + MOCK_CUSTOMER_ID_1 + "}} already exists", thrown.getMessage());

    }

    @Test
    void shouldReturnAllPhoneNumbersInTheSystem() {

        assertEquals(new HashSet<>(Arrays.asList(phoneNumberOne, phoneNumberTwo, phoneNumberThree, phoneNumberFour)), phoneNumberService.getAllPhoneNumbers());
    }

    @Test
    void shouldGetAllPhoneNumbersOfASingleCustomer() {
        assertEquals(new HashSet<>(List.of(phoneNumberTwo)), phoneNumberService.getAllPhoneNumbersByCustomerId(MOCK_CUSTOMER_ID_2));
    }

    @Test
    void shouldActivatePhoneNumberIfPhoneNumberExistsInTheDatabase() {
        assertEquals(phoneNumberService.getAllPhoneNumbersByCustomerId(MOCK_CUSTOMER_ID_2).stream().findFirst().get().getPhoneNumberStatus(), PhoneNumberStatus.INACTIVE);
        phoneNumberService.activatePhoneNumber(MOCK_PHONE_NUMBER_2);
        assertEquals(phoneNumberService.getAllPhoneNumbersByCustomerId(MOCK_CUSTOMER_ID_2).stream().findFirst().get().getPhoneNumberStatus(), PhoneNumberStatus.ACTIVE);
    }

    @Test
    void shouldThrowExceptionWhenInvalidNumberProvidedForActivation() {
        ActivationException thrown = Assertions
                .assertThrows(ActivationException.class, () -> phoneNumberService.activatePhoneNumber(MOCK_PHONE_NUMBER_NOT_IN_SYSTEM), "ActivationException error was expected");

        assertEquals(MOCK_PHONE_NUMBER_NOT_IN_SYSTEM + " not activated", thrown.getMessage());
    }
}
