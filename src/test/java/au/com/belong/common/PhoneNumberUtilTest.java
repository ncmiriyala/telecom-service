package au.com.belong.common;

import au.com.belong.domain.TestDataFixture;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PhoneNumberUtilTest {

    @Test
    void verifyIfPhoneNumberIsValid() {
        assertTrue(PhoneNumberUtil.isValidAustralianPhoneNumber("+61(02)89876544"));
        assertTrue(PhoneNumberUtil.isValidAustralianPhoneNumber("+61 2 8986 6544"));
        assertTrue(PhoneNumberUtil.isValidAustralianPhoneNumber("02 8986 6544"));
        assertTrue(PhoneNumberUtil.isValidAustralianPhoneNumber("+61289876544"));
        assertTrue(PhoneNumberUtil.isValidAustralianPhoneNumber("0414 570776"));
        assertTrue(PhoneNumberUtil.isValidAustralianPhoneNumber("0414570776"));
        assertTrue(PhoneNumberUtil.isValidAustralianPhoneNumber("0414 570 776"));
        assertTrue(PhoneNumberUtil.isValidAustralianPhoneNumber("04 1457 0776"));
        assertTrue(PhoneNumberUtil.isValidAustralianPhoneNumber("+61 414 570776"));
        assertTrue(PhoneNumberUtil.isValidAustralianPhoneNumber("+61 (04)14 570776"));
        assertTrue(PhoneNumberUtil.isValidAustralianPhoneNumber("+61 (04)14-570-776"));
    }

    @Test
    void verifyIfInvalidPhoneIsProvided() {
        assertFalse(PhoneNumberUtil.isValidAustralianPhoneNumber(TestDataFixture.MOCK_PHONE_NUMBER_NOT_IN_SYSTEM));
    }
}
