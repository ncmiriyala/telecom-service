package au.com.belong.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static au.com.belong.domain.TestDataFixture.MOCK_CUSTOMER_ID_1;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PhoneNumberTest {

    @Test
    void shouldThrowExceptionWhenInvalidPhoneNumberIsProvided() {
        AssertionError thrown = Assertions
                .assertThrows(AssertionError.class, () -> new PhoneNumber("34343434343", MOCK_CUSTOMER_ID_1), "AssertionError error was expected");

        assertEquals("Phone Number is invalid", thrown.getMessage());
    }

    @Test
    void verifyValidPhoneNumberIsCreatedWithInactiveStatus() {
        PhoneNumber phoneNumber = new PhoneNumber(TestDataFixture.MOCK_PHONE_NUMBER_1, MOCK_CUSTOMER_ID_1);
        assertEquals(phoneNumber.getNumber(), TestDataFixture.MOCK_PHONE_NUMBER_1);
        assertEquals(phoneNumber.getCustomer().getCustomerId(), MOCK_CUSTOMER_ID_1);
        assertEquals(phoneNumber.getPhoneNumberStatus(), PhoneNumberStatus.INACTIVE);
    }
}
