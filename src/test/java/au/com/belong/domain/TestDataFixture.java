package au.com.belong.domain;

public class TestDataFixture {
    public static final int MOCK_CUSTOMER_ID_1 = 123;
    public static final String MOCK_PHONE_NUMBER_1 = "0466098989";
    public static final int MOCK_CUSTOMER_ID_2 = 456;
    public static final String MOCK_PHONE_NUMBER_2 = "0422222222";
    public static final int MOCK_CUSTOMER_ID_3 = 789;
    public static final String MOCK_PHONE_NUMBER_3 = "0433333333";
    public static final String MOCK_PHONE_NUMBER_4= "02 8986 6544";
    public static final String MOCK_PHONE_NUMBER_NOT_IN_SYSTEM = "99999999999";
}
