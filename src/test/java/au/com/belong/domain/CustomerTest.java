package au.com.belong.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static au.com.belong.domain.TestDataFixture.*;
import static org.junit.jupiter.api.Assertions.*;

class CustomerTest {
    @Test
    void shouldThrowExceptionWhenInvalidCustomerIdIsProvided() {
        AssertionError thrown = Assertions
                .assertThrows(AssertionError.class, () -> new Customer(0), "AssertionError error was expected");

        assertEquals("CustomerId is invalid", thrown.getMessage());
    }

    @Test
    void verifyCustomerIsCreatedSuccessfully() {
        Customer customer = new Customer(MOCK_CUSTOMER_ID_1);
        customer.addPhoneNumber(new PhoneNumber(MOCK_PHONE_NUMBER_1, MOCK_CUSTOMER_ID_1));
        assertEquals(customer.getCustomerId(), MOCK_CUSTOMER_ID_1);
        Optional<PhoneNumber> optionalPhoneNumber = customer.getPhoneNumbers().stream().findFirst();
        assertEquals(optionalPhoneNumber.get().getNumber(), MOCK_PHONE_NUMBER_1);
        assertEquals(optionalPhoneNumber.get().getPhoneNumberStatus(), PhoneNumberStatus.INACTIVE);
    }
}
